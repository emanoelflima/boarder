(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _funcionario_funcionarios_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./funcionario/funcionarios.component */ "./src/app/funcionario/funcionarios.component.ts");
/* harmony import */ var _embarques_embarques_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./embarques/embarques.component */ "./src/app/embarques/embarques.component.ts");





var routes = [
    { path: 'funcionarios', component: _funcionario_funcionarios_component__WEBPACK_IMPORTED_MODULE_3__["FuncionariosComponent"] },
    { path: 'embarques', component: _embarques_embarques_component__WEBPACK_IMPORTED_MODULE_4__["EmbarquesComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app-style.scss":
/*!********************************!*\
  !*** ./src/app/app-style.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-icon {\n  padding: 0 14px; }\n\n.example-spacer {\n  flex: 1 1 auto; }\n\na {\n  color: black;\n  text-decoration: none; }\n\n.main-screen-icon {\n  font-size: 180px; }\n\n.main-toolbar {\n  background-image: url(\"https://i.redd.it/rhun61qvdvlz.png\");\n  background-position: center -530px;\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-color: black; }\n\n.main-toolbar * {\n  color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lbWFub2VsL0hhbGxpYnVydG9uL2JvYXJkZXIvc3JjL2FwcC9hcHAtc3R5bGUuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFlLEVBQ2xCOztBQUVEO0VBQ0ksZUFBYyxFQUNqQjs7QUFFRDtFQUNJLGFBQVk7RUFDWixzQkFBcUIsRUFDeEI7O0FBRUQ7RUFDSSxpQkFBZ0IsRUFDbkI7O0FBRUQ7RUFDSSw0REFBMkQ7RUFDM0QsbUNBQWtDO0VBQ2xDLDZCQUE0QjtFQUM1Qix1QkFBc0I7RUFDdEIsd0JBQXVCLEVBQzFCOztBQUVEO0VBQ0ksYUFBWSxFQUNmIiwiZmlsZSI6InNyYy9hcHAvYXBwLXN0eWxlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1pY29uIHtcbiAgICBwYWRkaW5nOiAwIDE0cHg7XG59XG4gIFxuLmV4YW1wbGUtc3BhY2VyIHtcbiAgICBmbGV4OiAxIDEgYXV0bztcbn1cblxuYXtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4ubWFpbi1zY3JlZW4taWNvbntcbiAgICBmb250LXNpemU6IDE4MHB4O1xufVxuXG4ubWFpbi10b29sYmFye1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnaHR0cHM6Ly9pLnJlZGQuaXQvcmh1bjYxcXZkdmx6LnBuZycpO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciAtNTMwcHg7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG4ubWFpbi10b29sYmFyICp7XG4gICAgY29sb3I6IHdoaXRlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


/**
 * Controller principal da aplicacao
 * @author Emanoel de Lima
 */
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'boarder';
        this.funcionarios = Array();
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.html */ "./src/app/app.html"),
            styles: [__webpack_require__(/*! ./app-style.scss */ "./src/app/app-style.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.html":
/*!**************************!*\
  !*** ./src/app/app.html ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar class=\"main-toolbar\">\n    <mat-toolbar-row>\n        <a href=\"./\">\n            <mat-icon>directions_boat</mat-icon>\n            <span>Boarder</span>\n        </a>\n        <span class=\"example-spacer\"></span>\n        <mat-icon class=\"example-icon\" matTooltip=\"Funcionários\" routerLinkActive=\"active\">\n            <a [routerLink]=\"['/funcionarios']\">assignment_ind</a>\n        </mat-icon>\n        <mat-icon class=\"example-icon\" matTooltip=\"Embarques\" routerLinkActive=\"active\">\n            <a [routerLink]=\"['/embarques']\" routerLinkActive=\"active\">swap_vert</a>\n        </mat-icon>\n    </mat-toolbar-row>\n\n</mat-toolbar>\n\n<mat-card class=\"main-card\">\n    <mat-card-header>\n        <mat-card-title>Bem-vindo ao {{title}}!</mat-card-title>\n    </mat-card-header>\n    <mat-card-content>\n        <mat-card-subtitle>Escolha uma das opções do menu para começar.</mat-card-subtitle>\n    </mat-card-content>\n</mat-card>\n<div style=\"text-align:center\">\n<h1>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _funcionario_funcionarios_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./funcionario/funcionarios.component */ "./src/app/funcionario/funcionarios.component.ts");
/* harmony import */ var _embarques_embarques_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./embarques/embarques.component */ "./src/app/embarques/embarques.component.ts");
/* harmony import */ var _funcionario_funcionario_service_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./funcionario/funcionario-service.service */ "./src/app/funcionario/funcionario-service.service.ts");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _funcionario_funcionarios_component__WEBPACK_IMPORTED_MODULE_10__["FuncionariosComponent"],
                _embarques_embarques_component__WEBPACK_IMPORTED_MODULE_11__["EmbarquesComponent"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatToolbarRow"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTooltipModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCardModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__["FlexLayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatNativeDateModule"]
            ],
            providers: [_funcionario_funcionario_service_service__WEBPACK_IMPORTED_MODULE_12__["FuncionariosService"], { provide: _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MAT_DATE_LOCALE"], useValue: 'pt-BR' }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/embarques/embarques-style.scss":
/*!************************************************!*\
  !*** ./src/app/embarques/embarques-style.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".searh-card {\n  background-color: #c0bebe; }\n\n.searh-card * {\n  font-weight: normal; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lbWFub2VsL0hhbGxpYnVydG9uL2JvYXJkZXIvc3JjL2FwcC9lbWJhcnF1ZXMvZW1iYXJxdWVzLXN0eWxlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwwQkFBeUIsRUFDNUI7O0FBRUQ7RUFDSSxvQkFBbUIsRUFDdEIiLCJmaWxlIjoic3JjL2FwcC9lbWJhcnF1ZXMvZW1iYXJxdWVzLXN0eWxlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2VhcmgtY2FyZHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzBiZWJlO1xufVxuXG4uc2VhcmgtY2FyZCAqe1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59Il19 */"

/***/ }),

/***/ "./src/app/embarques/embarques.component.html":
/*!****************************************************!*\
  !*** ./src/app/embarques/embarques.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-container\" fxLayout=\"row\" fxLayoutWrap fxLayoutGap=\"0.5%\" fxLayoutAlign=\"center\">\n  <mat-card class=\"searh-card\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\n    <mat-card-header>\n      <mat-card-title>Embarcar funcionário</mat-card-title>\n      <mat-card-subtitle>Informe código e o período de embarque desejado e clique em Salvar</mat-card-subtitle>\n    </mat-card-header>\n    <mat-card-content>\n      <mat-form-field>\n        <input matInput placeholder=\"Código\" [(ngModel)]=\"codigoFuncionario\" required>\n      </mat-form-field>\n      <mat-form-field>\n        <input matInput [matDatepicker]=\"picker\" placeholder=\"Embarque em\" [(ngModel)]=\"dataEmbarque\">\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n        <mat-datepicker #picker></mat-datepicker>\n      </mat-form-field>\n      <mat-form-field>\n          <input matInput [matDatepicker]=\"pickerDesembarque\" placeholder=\"Desembarque em\" [(ngModel)]=\"dataDesembarque\">\n          <mat-datepicker-toggle matSuffix [for]=\"pickerDesembarque\"></mat-datepicker-toggle>\n          <mat-datepicker #pickerDesembarque></mat-datepicker>\n        </mat-form-field>\n    </mat-card-content>\n    <mat-card-actions>\n      <button mat-button (click)=\"embarcar()\">Embarcar</button>\n    </mat-card-actions>\n  </mat-card>\n</div>"

/***/ }),

/***/ "./src/app/embarques/embarques.component.ts":
/*!**************************************************!*\
  !*** ./src/app/embarques/embarques.component.ts ***!
  \**************************************************/
/*! exports provided: EmbarquesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmbarquesComponent", function() { return EmbarquesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _funcionario_funcionario_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../funcionario/funcionario-service.service */ "./src/app/funcionario/funcionario-service.service.ts");



/**
 * Controller da pagina de embarques
 */
var EmbarquesComponent = /** @class */ (function () {
    function EmbarquesComponent(_service) {
        this._service = _service;
    }
    EmbarquesComponent.prototype.ngOnInit = function () {
    };
    /**
     * Atualiza os dados de embarque e desembarque de um funcionario.
     */
    EmbarquesComponent.prototype.embarcar = function () {
        if (this.validarCampos()) {
            var f = this._service.getById(this.codigoFuncionario);
            if (f != null) {
                f.dataEmbarque = this.dataEmbarque;
                f.dataDesembarque = this.dataDesembarque;
                this._service.salvarFuncionario(f);
                alert("Sucesso!");
            }
            else {
                alert("Funcionário de código " + this.codigoFuncionario + " não existe");
            }
        }
    };
    /**
     * Valida os campos obrigatorios do formulario
     * @returns resultado da validacao @type boolean
     */
    EmbarquesComponent.prototype.validarCampos = function () {
        if (this.codigoFuncionario == null) {
            alert("Código de funcionário é obrigatório.");
            return false;
        }
        return true;
    };
    EmbarquesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-embarques',
            template: __webpack_require__(/*! ./embarques.component.html */ "./src/app/embarques/embarques.component.html"),
            styles: [__webpack_require__(/*! ./embarques-style.scss */ "./src/app/embarques/embarques-style.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_funcionario_funcionario_service_service__WEBPACK_IMPORTED_MODULE_2__["FuncionariosService"]])
    ], EmbarquesComponent);
    return EmbarquesComponent;
}());



/***/ }),

/***/ "./src/app/funcionario/funcionario-service.service.ts":
/*!************************************************************!*\
  !*** ./src/app/funcionario/funcionario-service.service.ts ***!
  \************************************************************/
/*! exports provided: FuncionariosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuncionariosService", function() { return FuncionariosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);



/**
 * Servico de funcionarios
 * @author Emanoel de Lima
 */
var FuncionariosService = /** @class */ (function () {
    /**
     * Construtor do servico
     */
    function FuncionariosService() {
        this.x = moment__WEBPACK_IMPORTED_MODULE_2__();
        if (localStorage.getItem("funcionarios") == null) {
            localStorage.setItem("funcionarios", JSON.stringify(Array()));
        }
        this.funcionarios = JSON.parse(localStorage.getItem("funcionarios"));
    }
    /**
     * Obtem um funcionario por id.
     * @param id @type Number
     * @returns Funcionario
     */
    FuncionariosService.prototype.getById = function (id) {
        return this.funcionarios.find(function (x) { return x.id == id; });
    };
    /**
     * Obtem um funcionario por nome
     * @param nome
     * @return Funcionario
     */
    FuncionariosService.prototype.getByNome = function (nome) {
        return this.funcionarios.find(function (x) { return x.nome.toLowerCase() == nome.toLowerCase(); });
    };
    /**
     * Obtem todos os funcionarios cadastrados.
     * @returns List<Funcionario>
     */
    FuncionariosService.prototype.getTodos = function () {
        return this.funcionarios;
    };
    /**
     * Obtem um funcionario por data de embarque ou desembarque.
     * Se um dos filtros estiver vazio, busca os correspondentes sem a respectiva data.
     * @returns List<Funcionario>
     */
    FuncionariosService.prototype.getByDataEmbarque = function (dataEmbarque, dataDesembarque) {
        var momentDataEmbarque = moment__WEBPACK_IMPORTED_MODULE_2__(dataEmbarque).format("DDMMYYYY");
        var momentDataDesembarque = moment__WEBPACK_IMPORTED_MODULE_2__(dataDesembarque).format("DDMMYYYY");
        return this.funcionarios.filter(function (x) { return moment__WEBPACK_IMPORTED_MODULE_2__(x.dataEmbarque).format("DDMMYYYY") == momentDataEmbarque
            || moment__WEBPACK_IMPORTED_MODULE_2__(x.dataDesembarque).format("DDMMYYYY") == momentDataDesembarque; });
    };
    /**
     * Salva um funcionario, ou cria um novo.
     */
    FuncionariosService.prototype.salvarFuncionario = function (save) {
        var funcionario = this.getByNome(save.nome);
        if (funcionario == null) {
            save.id = this.funcionarios.length + 1;
            this.funcionarios.push(save);
        }
        else {
            funcionario.nome = save.nome;
            funcionario.empresa = save.empresa;
            funcionario.funcao = save.funcao;
        }
        localStorage.setItem("funcionarios", JSON.stringify(this.funcionarios));
    };
    FuncionariosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FuncionariosService);
    return FuncionariosService;
}());



/***/ }),

/***/ "./src/app/funcionario/funcionarios-style.scss":
/*!*****************************************************!*\
  !*** ./src/app/funcionario/funcionarios-style.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".funcionarios-table {\n  width: 100% !important; }\n\n.total-funcionarios {\n  font-size: 10px; }\n\n.funcionarios-table td {\n  text-align: left; }\n\n.general-card {\n  border: solid 1px #3b2a44; }\n\n.mat-card-header-text {\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lbWFub2VsL0hhbGxpYnVydG9uL2JvYXJkZXIvc3JjL2FwcC9mdW5jaW9uYXJpby9mdW5jaW9uYXJpb3Mtc3R5bGUuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHVCQUFzQixFQUN6Qjs7QUFFRDtFQUNJLGdCQUFlLEVBQ2xCOztBQUVEO0VBQ0ksaUJBQWdCLEVBQ25COztBQUVEO0VBQ0ksMEJBQWlDLEVBQ3BDOztBQUVEO0VBQ0ksWUFBVyxFQUNkIiwiZmlsZSI6InNyYy9hcHAvZnVuY2lvbmFyaW8vZnVuY2lvbmFyaW9zLXN0eWxlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVuY2lvbmFyaW9zLXRhYmxlIHtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG4udG90YWwtZnVuY2lvbmFyaW9ze1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbn1cblxuLmZ1bmNpb25hcmlvcy10YWJsZSB0ZHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4uZ2VuZXJhbC1jYXJke1xuICAgIGJvcmRlcjogc29saWQgMXB4IHJnYig1OSwgNDIsIDY4KTtcbn1cblxuLm1hdC1jYXJkLWhlYWRlci10ZXh0e1xuICAgIHdpZHRoOiAxMDAlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/funcionario/funcionarios.component.html":
/*!*********************************************************!*\
  !*** ./src/app/funcionario/funcionarios.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-container\" fxLayout=\"row\" fxLayoutWrap fxLayoutGap=\"0.5%\" fxLayoutAlign=\"center\">\n  <mat-card class=\"searh-card\" class=\"general-card\">\n    <mat-card-header>\n      <mat-card-title>Buscar funcionário</mat-card-title>\n      <mat-card-subtitle>Informe o período de embarque e clique no botão Buscar</mat-card-subtitle>\n    </mat-card-header>\n    <mat-card-content>\n      <mat-form-field>\n        <input matInput [matDatepicker]=\"picker\" placeholder=\"Embarque em\" [(ngModel)]=\"dataEmbarque\" date>\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n        <mat-datepicker #picker></mat-datepicker>\n      </mat-form-field>\n      <mat-form-field>\n        <input matInput [matDatepicker]=\"pickerDesembarque\" placeholder=\"Desembarque em\" [(ngModel)]=\"dataDesembarque\">\n        <mat-datepicker-toggle matSuffix [for]=\"pickerDesembarque\"></mat-datepicker-toggle>\n        <mat-datepicker #pickerDesembarque></mat-datepicker>\n      </mat-form-field>\n    </mat-card-content>\n    <mat-card-actions>\n      <button mat-button (click)=\"listarPorDataEmbarque()\">Buscar</button>\n      <button mat-button (click)=\"limparFiltro()\">Limpar campos</button>\n      <button mat-button (click)=\"getTodos()\">Exibir todos</button>\n    </mat-card-actions>\n  </mat-card>\n\n  <mat-card class=\"fields-card\" class=\"general-card\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"10px\">\n    <mat-card-header>\n      <mat-card-title>Novo funcionário</mat-card-title>\n      <mat-card-subtitle>Informe os dados e clique no botão Salvar</mat-card-subtitle>\n    </mat-card-header>\n\n    <mat-card-content>\n      <mat-form-field>\n        <input matInput placeholder=\"Nome\" [(ngModel)]=\"funcionarioSelecionado.nome\">\n      </mat-form-field>\n      \n      <mat-form-field>\n        <input matInput placeholder=\"Função\" [(ngModel)]=\"funcionarioSelecionado.funcao\">\n      </mat-form-field>\n      \n      <mat-form-field>\n        <input matInput placeholder=\"Empresa\" [(ngModel)]=\"funcionarioSelecionado.empresa\">\n      </mat-form-field>\n    </mat-card-content>\n\n    <mat-card-actions>\n      <button mat-button (click)=\"salvarFuncionario()\">Salvar</button>\n      <button mat-button (click)=\"limpar()\">Limpar campos</button>\n    </mat-card-actions>\n\n  </mat-card>\n</div><br />\n<div class=\"card-container\" fxLayout=\"row\" fxLayoutWrap fxLayoutAlign=\"center center\">\n  <mat-card class=\"card-list\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"10px\" fxFlex=\"100%\" fxFlexFill>\n    <table mat-table [dataSource]=\"funcionarios\" class=\"mat-elevation-z8 funcionarios-table\" style=\"width:100%\">\n\n      <ng-container matColumnDef=\"id\">\n        <th mat-header-cell *matHeaderCellDef> Código </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.id}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"nome\">\n        <th mat-header-cell *matHeaderCellDef> Nome </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.nome}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"funcao\">\n        <th mat-header-cell *matHeaderCellDef> Função </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.funcao}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"empresa\">\n        <th mat-header-cell *matHeaderCellDef> Empresa </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.empresa}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"dataEmbarque\">\n        <th mat-header-cell *matHeaderCellDef> Data de embarque </th>\n        <td mat-cell *matCellDef=\"let element\"> {{element.dataEmbarque | date:'dd-MM-yyyy'}} </td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"dataDesembarque\">\n          <th mat-header-cell *matHeaderCellDef> Data de desembarque </th>\n          <td mat-cell *matCellDef=\"let element\"> {{element.dataDesembarque | date:'dd-MM-yyyy'}} </td>\n        </ng-container>\n\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n    </table>\n  </mat-card>\n</div>\n\n<mat-card fxLayout=\"row\" fxLayoutAlign=\"center center\" fxLayoutGap=\"10px\">\n    <p>Total de funcionários / embarcados: {{totalFuncionarios}} / {{totalFuncionariosEmbarcados}}</p>\n</mat-card>"

/***/ }),

/***/ "./src/app/funcionario/funcionarios.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/funcionario/funcionarios.component.ts ***!
  \*******************************************************/
/*! exports provided: FuncionariosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuncionariosComponent", function() { return FuncionariosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _funcionario_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./funcionario-service.service */ "./src/app/funcionario/funcionario-service.service.ts");
/* harmony import */ var _model_Funcionario__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/Funcionario */ "./src/app/model/Funcionario.ts");




/**
 * Controller da pagina de funcionarios
 * @author Emanoel de Lima
 */
var FuncionariosComponent = /** @class */ (function () {
    /**
     * Construtor do controller
     * @param _service @type FuncionariosService
     */
    function FuncionariosComponent(_service) {
        this._service = _service;
        this.nomeBuscarFuncionario = "";
        this.dataEmbarque = null;
        this.dataDesembarque = null;
        this.funcionarios = Array();
        this.displayedColumns = ['id', 'nome', 'empresa', 'funcao', 'dataEmbarque', 'dataDesembarque'];
        this.funcionarioSelecionado = new _model_Funcionario__WEBPACK_IMPORTED_MODULE_3__["Funcionario"]("", "", "");
    }
    FuncionariosComponent.prototype.ngOnInit = function () {
        this.getTodos();
    };
    /**
     * Lista os funcionarios por datas de embarque e desembarque.
     */
    FuncionariosComponent.prototype.listarPorDataEmbarque = function () {
        var f = this._service.getByDataEmbarque(this.dataEmbarque, this.dataDesembarque);
        if (f == null) {
            alert("Não encontrado... :(");
        }
        else {
            this.funcionarios = f;
        }
        this.totalFuncionarios = this.funcionarios.length;
        this.totalFuncionariosEmbarcados = this.funcionarios.filter(function (x) { return x.dataEmbarque != null; }).length;
    };
    /**
     * Salva um funcionario, novo ou existente
     */
    FuncionariosComponent.prototype.salvarFuncionario = function () {
        this._service.salvarFuncionario(this.funcionarioSelecionado);
        alert("Sucesso!");
        if (this.dataDesembarque == null && this.dataEmbarque == null) {
            this.getTodos();
        }
        else {
            this.totalFuncionarios = this._service.getTodos().length;
        }
    };
    /**
     * Limpa os campos de busca e cadastro
     */
    FuncionariosComponent.prototype.limpar = function () {
        this.funcionarioSelecionado = new _model_Funcionario__WEBPACK_IMPORTED_MODULE_3__["Funcionario"]("", "", "");
        this.nomeBuscarFuncionario = "";
    };
    /**
     * Limpa os campos de filtros
     */
    FuncionariosComponent.prototype.limparFiltro = function () {
        this.dataDesembarque = null;
        this.dataEmbarque = null;
    };
    /**
     * Obtem todos os funcionarios e atualiza os contadores
     */
    FuncionariosComponent.prototype.getTodos = function () {
        this.funcionarios = this._service.getTodos();
        this.totalFuncionarios = this.funcionarios.length;
        this.totalFuncionariosEmbarcados = this.funcionarios.filter(function (x) { return x.dataEmbarque != null; }).length;
    };
    FuncionariosComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-funcionarios',
            template: __webpack_require__(/*! ./funcionarios.component.html */ "./src/app/funcionario/funcionarios.component.html"),
            styles: [__webpack_require__(/*! ./funcionarios-style.scss */ "./src/app/funcionario/funcionarios-style.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_funcionario_service_service__WEBPACK_IMPORTED_MODULE_2__["FuncionariosService"]])
    ], FuncionariosComponent);
    return FuncionariosComponent;
}());



/***/ }),

/***/ "./src/app/model/Funcionario.ts":
/*!**************************************!*\
  !*** ./src/app/model/Funcionario.ts ***!
  \**************************************/
/*! exports provided: Funcionario */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Funcionario", function() { return Funcionario; });
/**
 * Modelo que define os dados de um Funcionario.
 * @author Emanoel de Lima
 * Para manter o projeto simplificado, os dados de embarque e desembarque também ficarao nesta entidade.
 */
var Funcionario = /** @class */ (function () {
    function Funcionario(nome, funcao, empresa) {
        this.nome = nome;
        this.funcao = funcao;
        this.empresa = empresa;
        this.id = 0;
        this.dataEmbarque = null;
        this.dataDesembarque = null;
    }
    return Funcionario;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/emanoel/Halliburton/boarder/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map