import { Injectable } from '@angular/core';
import { Funcionario } from '../model/Funcionario';
import * as moment from 'moment';

/**
 * Servico de funcionarios
 * @author Emanoel de Lima
 */
@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {

  funcionarios : Array<Funcionario>;

  /**
   * Construtor do servico
   */
  constructor() {
    if(localStorage.getItem("funcionarios") == null){
      localStorage.setItem("funcionarios", JSON.stringify(Array<Funcionario>()));
    }
    this.funcionarios = JSON.parse(localStorage.getItem("funcionarios"));
  }
  
  x = moment();

  /**
   * Obtem um funcionario por id.
   * @param id @type Number
   * @returns Funcionario
   */
  getById(id){
    return this.funcionarios.find(x => x.id == id);
  }

  /**
   * Obtem um funcionario por nome
   * @param nome
   * @return Funcionario
   */
  getByNome(nome){
    return this.funcionarios.find(x => x.nome.toLowerCase() == nome.toLowerCase());
  }
  
  /**
   * Obtem todos os funcionarios cadastrados.
   * @returns List<Funcionario>
   */
  getTodos(){
    return this.funcionarios;
  }

  /**
   * Obtem um funcionario por data de embarque ou desembarque.
   * Se um dos filtros estiver vazio, busca os correspondentes sem a respectiva data.
   * @returns List<Funcionario>
   */
  getByDataEmbarque(dataEmbarque : Date, dataDesembarque : Date){
    let momentDataEmbarque = moment(dataEmbarque).format("DDMMYYYY");
    let momentDataDesembarque = moment(dataDesembarque).format("DDMMYYYY");
    return this.funcionarios.filter(
      x => moment(x.dataEmbarque).format("DDMMYYYY") == momentDataEmbarque
      || moment(x.dataDesembarque).format("DDMMYYYY") == momentDataDesembarque);
  }

  /**
   * Salva um funcionario, ou cria um novo.
   */
  salvarFuncionario(save : Funcionario){
    let funcionario : Funcionario = this.getByNome(save.nome);
    if(funcionario == null){
      save.id = this.funcionarios.length + 1;
      this.funcionarios.push(save);
    }
    else{
      funcionario.nome = save.nome;
      funcionario.empresa = save.empresa;
      funcionario.funcao = save.funcao;
    }
    localStorage.setItem("funcionarios", JSON.stringify(this.funcionarios));
  }

}
