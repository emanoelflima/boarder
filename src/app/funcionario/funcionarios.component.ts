import { Component, OnInit, Input } from '@angular/core';
import { FuncionariosService } from "./funcionario-service.service";
import { Funcionario } from '../model/Funcionario';
import * as moment from 'moment';

/**
 * Controller da pagina de funcionarios
 * @author Emanoel de Lima
 */
@Component({
  selector: 'app-funcionarios',
  templateUrl: './funcionarios.component.html',
  styleUrls: ['./funcionarios-style.scss']
})
export class FuncionariosComponent implements OnInit {

  funcionarioSelecionado : Funcionario;
  nomeBuscarFuncionario = "";
  dataEmbarque : Date = null;
  dataDesembarque : Date = null;
  funcionarios = Array<Funcionario>();
  totalFuncionarios : Number;
  totalFuncionariosEmbarcados : Number;
  displayedColumns = ['id', 'nome', 'empresa', 'funcao', 'dataEmbarque', 'dataDesembarque'];
  
  /**
   * Construtor do controller
   * @param _service @type FuncionariosService
   */
  constructor(private _service : FuncionariosService) { 
    this.funcionarioSelecionado = new Funcionario("","","");
  }
  
  ngOnInit() {
    this.getTodos();
  }

  /**
   * Lista os funcionarios por datas de embarque e desembarque.
   */
  listarPorDataEmbarque(){
      const f : Funcionario[] = this._service.getByDataEmbarque(this.dataEmbarque, this.dataDesembarque);
      if(f == null){
        alert("Não encontrado... :(");
      }
      else{
        this.funcionarios = f;
      }
      this.contarFuncionarios();
  }

  /**
   * Salva um funcionario, novo ou existente
   */
  salvarFuncionario(){
    this._service.salvarFuncionario(this.funcionarioSelecionado);
    alert("Sucesso!");
    if(this.dataEmbarque || this.dataDesembarque){
      this.listarPorDataEmbarque();
    }
    else{
      this.getTodos();
    }
  }

  /**
   * Limpa os campos de busca e cadastro
   */
  limpar(){
    this.funcionarioSelecionado = new Funcionario("","","");
    this.nomeBuscarFuncionario = "";
  }

  /**
   * Limpa os campos de filtros
   */
  limparFiltro(){
    this.dataDesembarque = null;
    this.dataEmbarque = null;
  }

  /**
   * Obtem todos os funcionarios e atualiza os contadores
   */
  getTodos(){
    this.funcionarios = [...this._service.getTodos()];
    this.contarFuncionarios();
  }

  /**
   * Conta os funcionários encontrados
   */
  contarFuncionarios(){
    this.totalFuncionarios = this.funcionarios.length;
    var now = moment().format("YYMMDD");
    this.totalFuncionariosEmbarcados = this.funcionarios.filter(
      x => x.dataEmbarque != null 
      && moment(x.dataEmbarque).format("YYMMDD") <= now 
      && x.dataDesembarque != null
      && moment(x.dataDesembarque).format("YYMMDD") > now
    ).length;
  }

}
