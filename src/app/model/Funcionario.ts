/**
 * Modelo que define os dados de um Funcionario.
 * @author Emanoel de Lima
 * Para manter o projeto simplificado, os dados de embarque e desembarque também ficarao nesta entidade.
 */
export class Funcionario {
    nome : String;
    funcao : String;
    empresa : String;
    id : Number;
    dataEmbarque : Date;
    dataDesembarque : Date;
    
    constructor(nome, funcao, empresa){
        this.nome = nome;
        this.funcao = funcao;
        this.empresa = empresa;
        this.id = 0;
        this.dataEmbarque = null;
        this.dataDesembarque = null;
    }

}