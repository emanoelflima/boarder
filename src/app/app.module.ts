import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTooltipModule, MatFormFieldModule, MatInputModule, MatCardModule, MatTableModule, MatDatepickerModule, MatNativeDateModule, MatToolbarRow, MatIconModule, MatToolbarModule } from '@angular/material';
import { MatButtonModule } from "@angular/material/button";
import { FormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FuncionariosComponent } from './funcionario/funcionarios.component';
import { EmbarquesComponent } from './embarques/embarques.component';
import { FuncionariosService } from './funcionario/funcionario-service.service';

import {MAT_DATE_LOCALE} from '@angular/material/core';

@NgModule({
  declarations: [
    AppComponent,
    FuncionariosComponent,
    EmbarquesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    FlexLayoutModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatToolbarModule
  ],
  providers: [FuncionariosService, {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
