import { Injectable } from '@angular/core';

/**
 * Servico da tela de Embarques
 * @author Emanoel de Lima
 * Nao deixei nenhum método aqui, porque embarque e desembarque ficou junto ao objeto Funcionario.
 * Em um sistema com persistência de dados em um banco, esse servico seria responsavel por inciar
 * a persistência em alguma tabela de embarques, que teria um relacionamento com a tabela de funcionários.
 * Como está além desse projeto simplificado e um servico não poderia acessar métodos de outro, deixei a
 * responsabilidade dos dados de embarque e desembarque com o servico de Funcionarios.
 */
@Injectable({
  providedIn: 'root'
})
export class EmbarquesServiceService {

  constructor() { }
}
