import { Component, OnInit } from '@angular/core';
import { FuncionariosService } from "../funcionario/funcionario-service.service";
import { Funcionario } from '../model/Funcionario';
import * as moment from 'moment';

/**
 * Controller da pagina de embarques
 */
@Component({
  selector: 'app-embarques',
  templateUrl: './embarques.component.html',
  styleUrls: ['./embarques-style.scss']
})
export class EmbarquesComponent implements OnInit {

  codigoFuncionario : Number;
  displayedColumns = ['id', 'nome', 'empresa', 'funcao', 'dataEmbarque', 'embarcar', 'dataDesembarque', 'desembarcar', 'salvar'];

  funcionarios = Array<Funcionario>();

  constructor(private _service : FuncionariosService) { }

  ngOnInit() {
    this.getTodos();
  }

  /**
   * Atualiza os dados de embarque e desembarque de um funcionario.
   */
  embarcar(funcionario : Funcionario){
    if(this.validarCampos(funcionario)){
      this._service.salvarFuncionario(funcionario);
      alert("Sucesso!");
    }
  }

  /**
   * Valida os campos obrigatorios do formulario
   * @returns resultado da validacao @type boolean
   */
  validarCampos(funcionario : Funcionario){
    if(funcionario.dataEmbarque != null && funcionario.dataDesembarque != null){
      var embarque = moment(funcionario.dataEmbarque).format("YYMMDD");
      var desembarque = moment(funcionario.dataDesembarque).format("YYMMDD");
      if(embarque >= desembarque){
        alert("Data de desembarque deve ser pelo menos um dia após data de embarque.");
        return false;
      }
    }
    else{
      alert("É necessário informar as duas datas para prosseguir.");
      return false;
    }
    return true;
  }

  /**
   * Obtem todos os funcionarios e atualiza os contadores
   */
  getTodos(){
    this.funcionarios = this._service.getTodos();
  }

}
