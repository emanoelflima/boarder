import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FuncionariosComponent } from './funcionario/funcionarios.component';
import { EmbarquesComponent } from './embarques/embarques.component';

const routes: Routes = [
  { path: 'funcionarios', component: FuncionariosComponent },
  { path: 'embarques', component: EmbarquesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
