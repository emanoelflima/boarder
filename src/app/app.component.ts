import { Component } from '@angular/core';
import { Funcionario } from "./model/Funcionario";

/**
 * Controller principal da aplicacao
 * @author Emanoel de Lima
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.html',
  styleUrls: ['app-style.scss']
})
export class AppComponent {
  title = 'boarder';
  funcionarios = Array<Funcionario>();
}
